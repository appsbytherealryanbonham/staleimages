<?php

/**
 * MIT License
 *
 * Copyright (c) 2021 Ryan Bonham
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

function staleimages_install_addon()
{
    global $conn, $config, $misc;

    $current_version = staleimages_current_version();

    $sql = 'SELECT addons_version FROM ' . $config['table_prefix_no_lang']
        . 'addons WHERE addons_name = \'staleimages\'';
    $recordSet = $conn->Execute($sql);
    $version = $recordSet->fields[0];
    $sql = array();

    if ($version == '') {
        $sql[] = 'INSERT INTO ' . $config['table_prefix_no_lang']
            . 'addons (addons_version, addons_name) VALUES (' . $misc->make_db_safe($current_version) . ',\'staleimages\')';

        foreach ($sql as $s) {
            $recordSet = $conn->Execute($s);
        }
        return null;
    }
    $sql = array();
    if ($version != $current_version) {
        switch ($version) {
            case '1':

            default:
                $sql[] = 'UPDATE ' . $config['table_prefix_no_lang']
                    . 'addons SET addons_version = ' . $misc->make_db_safe($current_version) . ' WHERE addons_name = \'staleimages\'';
                break;
        }
    }
    foreach ($sql as $s) {
        $recordSet = $conn->Execute($s);
        if (!$recordSet) {
            $misc->log_error($s);
        }
    }
}

function staleimages_show_admin_icons()
{
    global $config;
    require_once($config['basepath'] . '/include/login.inc.php');
    $login = new login();
    $admin_link = array();
    // Check for tags: Admin, Agent, canEditForms, canViewLogs, editpages, havevtours
    $login_status = $login->verify_priv('Admin');

    if ($login_status !== false) {
        $admin_link[] =
            '<a href="index.php?action=addon_staleimages_find"><img src="' . $config['baseurl'] . '/addons/staleimages/assets/imgs/find.png" /><br />Find Stale Images</a>';
        $admin_link[] =
            '<a href="index.php?action=addon_staleimages_remove"><img src="' . $config['baseurl'] . '/addons/staleimages/assets/imgs/remove.png" /><br />Remove Stale Images</a>';
    }

    return $admin_link;
}

function staleimages_load_template()
{
    $template_array = array('');
    return $template_array;
}

function staleimages_run_action_user_template()
{
    $_GET['action'];

    switch (false) {
        default:
            $data = '';
            break;
    }

    return $data;
}

function staleimages_run_action_admin_template()
{
    global $config;
    global $conn;
    ini_set('max_execution_time', 0);
    $data = '';
    $data .= staleimages_show_header();
    switch ($_GET['action']) {
        case 'addon_staleimages_find':
            $data .= staleimages_find();
            break;
        case 'addon_staleimages_remove':
            $data .= staleimages_find(false);
            break;
    }

    $data .= staleimages_show_footer();
    return $data;
}
function staleimages_current_version()
{
    $current_version = '1.1.1';
    return $current_version;
}
function staleimages_show_header()
{
    global $config, $loadjs;

    $display = '<div style="background-repeat:no-repeat;text-align:right;height:50px; width:100%;display:block;background-image:url(' . $config['baseurl'] . '/addons/staleimages/find.png);">';
    $display .= 'Stale Image Remover  v' . staleimages_current_version() . '<br/>';



    $display .= '</div><div style="margin:3px;">';
    return $display;
}

function staleimages_show_footer()
{
    $display =
        '</div><div style="width:100%;">Stale Image Remover &copy;2022 <a href="https://gitlab.com/appsbytherealryanbonham" target="_blank" rel="noopener noreferrer">Ryan Bonham</a></div>';
    return $display;
}
function staleimages_find($report_only = true)
{
    global $config, $conn, $misc;
    $display = '';
    //First Look for Images in the Open-Realty database that do not have a listing anymore
    $sql = 'SELECT * FROM ' . $config['table_prefix'] . 'listingsimages WHERE listingsdb_id NOT IN (SELECT listingsdb_id from ' . $config['table_prefix'] . 'listingsdb);';
    $staledbimages = $conn->Execute($sql);
    if (!$staledbimages) {
        $misc->log_error($sql);
    }
    if ($staledbimages->RecordCount() == 0) {
        $display .= '<span style="color:green;">GOOD</style> - No Images in database that do not have a listing.<br />';
    } else {
        $display .= '<span style="color:red;">BAD</style> - ' . $staledbimages->RecordCount() . ' image(s) in database that do not have a listing.<br />';
        if (!$report_only) {
            while (!$staledbimages->EOF) {
                $imagesdbid = $staledbimages->fields['listingsimages_id'];
                $fullimagename = $staledbimages->fields['listingsimages_file_name'];
                $thumbname = $staledbimages->fields['listingsimages_thumb_file_name'];
                if (file_exists("$config[listings_upload_path]/$fullimagename")) {
                    if (!unlink("$config[listings_upload_path]/$fullimagename")) {
                        $display .= '<span style="color:red;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FAILURE</style> - Failed to remove image ' . $config['listings_upload_path'] . '/' . $fullimagename . '<br />';
                    }
                } else {
                    $display .= '<span style="color:yellow;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WARNING</style> - Image ' . $config['listings_upload_path'] . '/' . $fullimagename . ' was in database but was not really present.<br />';
                }
                //Remove Thumbnail
                if ($thumbname !== $fullimagename) {
                    if (file_exists("$config[listings_upload_path]/$thumbname")) {
                        if (!unlink("$config[listings_upload_path]/$thumbname")) {
                            $display .= '<span style="color:red;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FAILURE</style> - Failed to remove image ' . $config['listings_upload_path'] . '/' . $thumbname . '<br />';
                        }
                    } else {
                        $display .= '<span style="color:yellow;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WARNING</style> - Image ' . $config['listings_upload_path'] . '/' . $thumbname . ' was in database but was not really present.<br />';
                    }
                }
                //Remove from SQL
                $sql = 'DELETE FROM ' . $config['table_prefix'] . 'listingsimages WHERE listingsimages_id = ' . $imagesdbid;
                $removeimage = $conn->Execute($sql);
                if (!$removeimage) {
                    $misc->log_error($sql);
                }
                $staledbimages->MoveNext();
            }
        }
    }
    //Ok Now let work on disk
    //Get array of all images on disk
    $files = array();
    if ($handle = opendir($config['listings_upload_path'])) {
        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != ".." && $file != '.svn' && $file != 'index.html' && !is_dir($file)) {
                $files[$file] = $file;
            }
        }
        closedir($handle);
    }
    $fullimagecount = count($files);
    $display .= '<span style="color:gold;">INFO</style> - Found ' . $fullimagecount . ' images on disk.<br />';
    $sql = 'SELECT * FROM ' . $config['table_prefix'] . 'listingsimages;';
    $stalediskimages = $conn->Execute($sql);
    if (!$stalediskimages) {
        $misc->log_error($sql);
    }
    //Build List of Images to look for images in DB not on disk
    $image_dbarray = array();

    while (!$stalediskimages->EOF) {
        $fullimagename = $stalediskimages->fields['listingsimages_file_name'];
        $thumbname = $stalediskimages->fields['listingsimages_thumb_file_name'];
        if (strpos($fullimagename, 'http://') !== 0 && strpos($fullimagename, 'https://') !== 0) {
            $image_dbarray[] = $fullimagename;
            $image_dbarray[] = $thumbname;
        }
        $stalediskimages->MoveNext();
    }
    $imagesmissingdisk = array_diff($image_dbarray, $files);
    $staleimagecount = count($imagesmissingdisk);
    $removedcount = 0;
    if ($staleimagecount > 0) {
        $display .= '<span style="color:red;">BAD</style> - Found ' . $staleimagecount . ' stale images in DB.<br />';
        if (!$report_only) {
            foreach ($imagesmissingdisk as $file) {
                $sql = 'DELETE FROM ' . $config['table_prefix'] . 'listingsimages WHERE listingsimages_file_name = ' . $misc->make_db_safe($file) . ' OR listingsimages_thumb_file_name  = ' . $misc->make_db_safe($file);
                $stalediskimages = $conn->Execute($sql);
                if (!$stalediskimages) {
                    $misc->log_error($sql);
                }
                $removedcount++;
            }
            $display .= '<span style="color:green;">GREEN</style> - ' . $removedcount . ' stale images removed.<br />';
        }
    } else {
        $display .= '<span style="color:green;">GREEN</style> - No stale images in DB.<br />';
    }

    $sql = 'SELECT * FROM ' . $config['table_prefix'] . 'listingsimages;';
    $stalediskimages = $conn->Execute($sql);
    if (!$stalediskimages) {
        $misc->log_error($sql);
    }
    //Ok Now look for images on disk that are no in DB
    while (!$stalediskimages->EOF) {
        $fullimagename = $stalediskimages->fields['listingsimages_file_name'];
        $thumbname = $stalediskimages->fields['listingsimages_thumb_file_name'];
        unset($files[$fullimagename]);
        unset($files[$thumbname]);
        $stalediskimages->MoveNext();
    }
    $staleimagecount = count($files);
    $removedcount = 0;
    if ($staleimagecount > 0) {
        $display .= '<span style="color:red;">BAD</style> - Found ' . $staleimagecount . ' stale images on disk.<br />';
        if (!$report_only) {
            foreach ($files as $file) {
                if (!unlink("$config[listings_upload_path]/$file")) {
                    $display .= '<span style="color:red;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FAILURE</style> - Failed to remove image ' . $config['listings_upload_path'] . '/' . $file . '<br />';
                } else {
                    $removedcount++;
                }
            }
            $display .= '<span style="color:green;">GREEN</style> - ' . $removedcount . ' stale images removed.<br />';
        }
    } else {
        $display .= '<span style="color:green;">GREEN</style> - No stale images on disk.<br />';
    }




    return $display;
}
