# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] - ####-##-###

Here we write upgrading notes for open-realty. It's a team effort to make them as
straightforward as possible.

## [1.1.1] - 2022-02-03

### Fixed

- StaleImages removal process now supports remote images, and will not delete them from the DB.

## [1.1.0] - 2022-02-03

### Added

- PHP 7.4 support
- Initial Release under MIT license
