# StaleImages

**Description**: StaleImages is a map add-on for [Open-Realty](https://www.open-realty.org) that find and removes images in Open-Realty that are missing in either the DB or the filesystem.

## Dependencies

- PHP v7.4.3+
- [Open-Realty](https://www.open-realty.org) 3.4.0+

## Installation

Coming Soon

## Getting help

For general help, try the [Open-Realty discord server](https://discord.gg/uU7EYnxW).
If you have feature request or bug reports, etc, please file an issue in this repository's Issue Tracker.

## Getting involved

See our [CONTRIBUTING](CONTRIBUTING.md) guide.

---

## Open source licensing info

[MIT LICENSE](LICENSE)
